/*---------------------------------------------------------*\
| OpenRGBDeskFan                                            |
|                                                           |
|   Firmware for OpenRGB Desk Fan                           |
|                                                           |
|   Adam Honse (calcprogrammer1@gmail.com)      10 Jun 2024 |
|                                                           |
|   Based on CorsairLightingProtocol CommanderPRO           |
|   Leon Kiefer                                        2019 |
|                                                           |
|   SPDX-License-Identifier: Apache-2.0                     |
\*---------------------------------------------------------*/

#include <Button.h>
#include <CorsairLightingProtocol.h>
#include <Encoder.h>
#include <FastLED.h>

#include "SimpleFanController.h"
#include "ThermistorTemperatureController.h"

/*---------------------------------------------------------*\
| Constants                                                 |
\*---------------------------------------------------------*/
#define CHANNEL_LED_COUNT     48

#define FAN_POWER_PIN         10
#define FAN_UPDATE_RATE       500

#define KNOB_COUNTS_PER_CLICK 4
#define KNOB_PIN_1            16
#define KNOB_PIN_2            14
#define BUTTON_PIN            15

#define LED_PIN_CHANNEL_1     2
#define LED_PIN_CHANNEL_2     4

#define PWM_FAN_PIN_1         5

enum
{
  KNOB_MODE_FAN_POWER,
  KNOB_MODE_LED_BRIGHTNESS
};

enum
{
  MODE_COLOR_FAN_POWER      = 0x00FF0000,
  MODE_COLOR_LED_BRIGHTNESS = 0x0000FF00
};

/*---------------------------------------------------------*\
| Global variables                                          |
\*---------------------------------------------------------*/
unsigned long   last_knob_change_time;
int32_t         last_knob_pos;
CRGB            ledsChannel1[CHANNEL_LED_COUNT];
CRGB            ledsChannel2[CHANNEL_LED_COUNT];
unsigned char   knob_mode;

/*---------------------------------------------------------*\
| Constant Variables                                        |
\*---------------------------------------------------------*/
const char      mySerialNumber[] PROGMEM = "ORGBDSKFAN0000";

/*---------------------------------------------------------*\
| CorsairLightingProtocol Setup                             |
\*---------------------------------------------------------*/
CorsairLightingFirmwareStorageEEPROM firmwareStorage;
CorsairLightingFirmware firmware(CORSAIR_COMMANDER_PRO, &firmwareStorage);
ThermistorTemperatureController temperatureController;
FastLEDControllerStorageEEPROM storage;
FastLEDController ledController(&storage);
SimpleFanController fanController(&temperatureController, FAN_UPDATE_RATE, EEPROM_ADDRESS + storage.getEEPROMSize());
CorsairLightingProtocolController cLP(&ledController, &temperatureController, &fanController, &firmware);
CorsairLightingProtocolHID cHID(&cLP, mySerialNumber);

/*---------------------------------------------------------*\
| Fan Setup                                                 |
\*---------------------------------------------------------*/
PWMFan fan1(PWM_FAN_PIN_1, 0, 2000);

/*---------------------------------------------------------*\
| Knob Setup                                                |
\*---------------------------------------------------------*/
Encoder knob(KNOB_PIN_1, KNOB_PIN_2);

/*---------------------------------------------------------*\
| Button Setup                                              |
\*---------------------------------------------------------*/
Button button(BUTTON_PIN);

/*---------------------------------------------------------*\
| Setup                                                     |
\*---------------------------------------------------------*/
void setup()
{
  /*-------------------------------------------------------*\
  | Add LEDs to FastLED                                     |
  \*-------------------------------------------------------*/
	FastLED.addLeds<WS2812B, LED_PIN_CHANNEL_1, GRB>(ledsChannel1, CHANNEL_LED_COUNT);
	FastLED.addLeds<WS2812B, LED_PIN_CHANNEL_2, GRB>(ledsChannel2, CHANNEL_LED_COUNT);

  /*-------------------------------------------------------*\
  | Add LEDs to CLP LED Controllers                         |
  \*-------------------------------------------------------*/
	ledController.addLEDs(0, ledsChannel1, CHANNEL_LED_COUNT);
	ledController.addLEDs(1, ledsChannel2, CHANNEL_LED_COUNT);

  /*-------------------------------------------------------*\
  | Set fan power pin to output                             |
  \*-------------------------------------------------------*/
  pinMode(FAN_POWER_PIN, OUTPUT);
  
  /*-------------------------------------------------------*\
  | Add fan to CLP Fan Controller                           |
  \*-------------------------------------------------------*/
	fanController.addFan(0, &fan1);

  /*-------------------------------------------------------*\
  | Start Button                                            |
  \*-------------------------------------------------------*/
  button.begin();

  /*-------------------------------------------------------*\
  | Initialize knob mode to Fan Power                       |
  \*-------------------------------------------------------*/
  knob_mode = KNOB_MODE_FAN_POWER;

  /*-------------------------------------------------------*\
  | Initialize knob position to the saved fan power         |
  \*-------------------------------------------------------*/
  load_knob();
}

/*---------------------------------------------------------*\
| Loop                                                      |
\*---------------------------------------------------------*/
void loop()
{
  /*-------------------------------------------------------*\
  | Update CLP                                              |
  \*-------------------------------------------------------*/
	cHID.update();

  /*-------------------------------------------------------*\
  | Read button                                             |
  \*-------------------------------------------------------*/
  if(button.pressed())
  {
    /*-----------------------------------------------------*\
    | Only toggle mode if within 5 seconds of knob change   |
    | or button press                                       |
    \*-----------------------------------------------------*/
    if( ( millis() - last_knob_change_time ) < 5000 )
    {
      knob_mode += 1;
  
      if(knob_mode > 1)
      {
        knob_mode = 0;
      }
    }

    /*-----------------------------------------------------*\
    | Initialize knob position to the selected mode         |
    \*-----------------------------------------------------*/
    load_knob();

    /*-----------------------------------------------------*\
    | Update last knob change time                          |
    \*-----------------------------------------------------*/
    last_knob_change_time = millis();
  }
  
  /*-------------------------------------------------------*\
  | Read knob                                               |
  \*-------------------------------------------------------*/
  int32_t current_knob_pos = knob.read();

  current_knob_pos = current_knob_pos / KNOB_COUNTS_PER_CLICK;

  if( current_knob_pos < 0 )
  {
    current_knob_pos = 0;
    knob.write(0);
  }

  if( current_knob_pos > 10 )
  {
    current_knob_pos = 10;
    knob.write(current_knob_pos * KNOB_COUNTS_PER_CLICK);
  }

  /*-------------------------------------------------------*\
  | Update last knob change time if knob changed            |
  \*-------------------------------------------------------*/
  if( current_knob_pos != last_knob_pos )
  {
    last_knob_change_time = millis();
  }
  
  last_knob_pos = current_knob_pos;

  /*-------------------------------------------------------*\
  | If knob has changed within 5s, show fan speed on LEDs   |
  \*-------------------------------------------------------*/
  if( ( millis() - last_knob_change_time ) < 5000 )
  {
    /*---------------------------------------------------*\
    | Select LED color based on knob mode                 |
    \*---------------------------------------------------*/
    CRGB color = 0x00000000;

    switch(knob_mode)
    {
      case KNOB_MODE_FAN_POWER:
        color = MODE_COLOR_FAN_POWER;
        break;

      case KNOB_MODE_LED_BRIGHTNESS:
        color = MODE_COLOR_LED_BRIGHTNESS;
        break;
    }

    /*---------------------------------------------------*\
    | Light up fan in the selected mode color             |
    | Reset LED strip to off                              |
    \*---------------------------------------------------*/
    for( int i = 0; i < CHANNEL_LED_COUNT; i++ )
    {
      ledsChannel1[ i ] = color;
      ledsChannel2[ i ] = 0x00000000;
    }

    /*---------------------------------------------------*\
    | Display knob setting on LED strip                   |
    \*---------------------------------------------------*/
    for( int i = 0; i < 10; i++ )
    {
      if( i >= current_knob_pos )
      {
        ledsChannel2[ i ] = 0x00000000;
      }
      else
      {
        ledsChannel2[ i ] = color;
      }
    }

    /*---------------------------------------------------*\
    | Scale knob position to 0-255                        |
    \*---------------------------------------------------*/
    int knob_scaled = current_knob_pos * 26;
    
    if( knob_scaled > 255 )
    {
      knob_scaled = 255;
    }

    /*---------------------------------------------------*\
    | Set fan power or LED brightness based on knob       |
    | position and mode                                   |
    \*---------------------------------------------------*/
    switch(knob_mode)
    {
      case KNOB_MODE_FAN_POWER:
        fanController.setFanPower(0, knob_scaled);
        break;
  
      case KNOB_MODE_LED_BRIGHTNESS:
        FastLED.setBrightness(knob_scaled);
        break;
    }
    
    FastLED.show();
  }
  /*-------------------------------------------------------*\
  | Otherwise, process CLP LED controller                   |
  \*-------------------------------------------------------*/
  else if(ledController.updateLEDs())
  {
    FastLED.show();
  }

  /*-------------------------------------------------------*\
  | Update fan controller                                   |
  \*-------------------------------------------------------*/
  if(fanController.getFanPower(0) > 0)
  {
    digitalWrite(FAN_POWER_PIN, HIGH);
  }
  else
  {
    digitalWrite(FAN_POWER_PIN, LOW);
  }
  
	fanController.updateFans();
}

void load_knob()
{
  /*-----------------------------------------------------*\
  | Load the selected mode value into the knob            |
  \*-----------------------------------------------------*/
  uint8_t value;
    
  switch(knob_mode)
  {
    case KNOB_MODE_FAN_POWER:
      value = fanController.getFanPower(0);
      break;

    case KNOB_MODE_LED_BRIGHTNESS:
      value = FastLED.getBrightness();
      break;
  }

  /*-----------------------------------------------------*\
  | If remainder is greater than half, add one (round up) |
  \*-----------------------------------------------------*/
  if(value % 26 > 13)
  {
    knob.write(((value / 26) + 1) * KNOB_COUNTS_PER_CLICK);
  }
  else
  {
    knob.write(((value / 26)) * KNOB_COUNTS_PER_CLICK);
  }
}
