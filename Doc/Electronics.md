# OpenRGB Desk Fan Electronics

The electronics for this project are built around an Arduino Pro Micro running a modified version of the CorsairLightingProtocol firmware.  To connect the Arduino to the rest of the components, I chose to use commonly available 5x7cm prototyping PCBs.  The Arduino is soldered on to the PCB along with other components.  The transistors and resistors are used to form a transistor switch circuit that switches the 12V power to the fan, allowing the fan to be completely powered off when at the 0% setting.  Without this circuit, many PWM fans will continue to spin slowly even at 0% command.

Two versions of the electronics are available for this project.  The first is powered by a 12V external power supply and the second is entirely powered by the USB 5V supply.  While the USB powered version only requires a single connection to the computer and no external power, it does technically exceed the USB 500mA current limit by quite a bit.  However, most USB ports seem fine with the extra current draw - measured up to around 1.2A at full fan speed and full brightness white - so it's up to you which one you choose for your build.  If you plan to connect the OpenRGB fan to a USB hub, especially an unpowered USB hub, you should opt for the 12V externally powered version.

## Common Components

This parts list applies to both the 12V Externally Powered and USB Powered versions:

### PCB

* Arduino Pro Micro, USB-C Variant (https://www.amazon.com/dp/B0B6HYLC44)
* 5x7cm Prototyping PCB (https://www.amazon.com/dp/B00FXHXT80)
* 0.1 Inch (2.54mm) Male Header Pins (3 4-pin and 1 3-pin segments) (https://www.amazon.com/dp/B06ZZN8L9S)
* 2N2907 PNP Transistor (https://www.amazon.com/dp/B0CQXRP24H/)
* 2N2222 NPN Transistor (above link includes both transistors)
* 2x 10K Ohm Resistor (https://www.amazon.com/dp/B08FD1XVL6)
* 1K Ohm Resistor (above link includes both resistor values)

### Other

* 360 Degree Rotary Encoder With Push Button (https://www.amazon.com/dp/B07DM2YMT4)
* 0.1 Inch (2.54mm) Female Header Connector (1 4-pin segment)
* If using an ARGB fan with a standard ARGB header, you will need a Corsair 4-pin to ARGB 3-pin adapter (https://www.amazon.com/dp/B0C7FNQ6NC)

## 12V Externally Powered Version

### Additional Components

* LM2596S Adjustable Step Down DC-DC Buck Converter Regulator Module (https://www.amazon.com/dp/B0CPBBGWC3)
* 5.5mm PCB Mount Barrel Jack Socket (https://www.amazon.com/dp/B00W944ACE)

I used 1-pin segments of pin header on each of the module's connections to solder it down to the PCB.  As the module is larger than the remaining space on the PCB, I had to use a second PCB.  Align the pins of the module across the two boards as shown, leaving a slight gap between the boards.  This should allow the completed assembly to fit into the 3D-printed housing.  As for the barrel jack, the one I used did not have 0.1 inch spaced pins so instead of soldering it directly, I glued it in place as shown, then soldered bare wire to the positive and negative terminals.  I glued it by cutting a small piece of hot glue stick and melting it with my hot air reflow station, then sticking the connector down and lining it up with the printed line on the PCB.  Note the orientation of the two PCBs if you plan on using the markings.  Adjust the module to output 5V before connecting the fan or RGB strip.

![image](12VPowered_Front.jpg)
![image](12VPowered_Back.jpg)

## USB Powered Version

### Additional Components

* MT3608 Adjustable Step Up DC-DC Boost Converter Regulator Module (https://www.amazon.com/dp/B089JYBF25)
* 6.3V 1800uF Electrolytic Capacitor (https://www.amazon.com/dp/B010TN8HCA)

I used 1-pin segments of pin header on each of the module's connections to solder it down to the PCB.  To provide additional 5V power supply stability, I added a capacitor.  To fit it in the housing, I bent the leads down and soldered it in sideways as shown.  Adjust the module to output 12V before connecting the fan.

Additionally, you will need to do some modifications to the Arduino for this version of the design.  First, you will need to short the J1 jumper (J1 in image below) next to the USB connector by soldering it closed.  Next, you will need to short out the polyfuse (PF) by soldering a short piece of wire across it (or by desoldering it entirely and replacing it with a wire link).  This allows the Ardino to pull more than 500mA.

NOTE:  When I bypassed the polyfuse (PF) on my Arduino, something damaged one of the two yellow tantalum capacitors (CAP), likely heat from the soldering iron.  It shorted out and the Arduino would not power on until I removed it.  It gets bypassed by the J1 jumper so it can be safely removed.

![image](USBPowered_Front.jpg)
![image](USBPowered_Back.jpg)
![image](ArduinoMods.jpg)

## Connecting the PCB

The image below shows the connections on the PCB.  The connections are the same regardless of which version you use.  The topmost connector on the left is the ARGB connection for the fan and it is designed for a Corsair fan which uses a 4-pin 0.1-spaced connector.  The tab on the connector should face the Arduino.  The middle connector on the left is the ARGB connection for the 10 LED ARGB strip.  Match the pinout according to the labels on the image.  The bottom connector on the left is for the 4-pin 12V PWM fan.  The tabs on the connector should face the regulator module.  The connector in the middle is to connect the encoder knob.  It has four pins - Encoder A and B, Button, and Ground.

![image](Pinout.jpg)
![image](Connections.jpg)

## Connecting the Encoder

To wire the encoder, I used a 4-pin female pin header and soldered wires.  The Encoder A and B are the left and right pins of the side of the encoder knob with 3 pins.  The center pin is ground.  A jumper from ground has to go over to one of the two pins on the other side to ground the push button as well.  The other side of the button is wired to the Button input.

![image](Wiring_EncoderConnector.jpg)
![image](Wiring_Encoder.jpg)
