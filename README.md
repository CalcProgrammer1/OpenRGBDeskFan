# OpenRGB Desk Fan

![image](Doc/OpenRGBDeskFan.jpg)

This project is a desk fan inspired by the Noctua desk fan, which is basically a stand and an external power supply/PWM controller for a PC fan.  I wanted to do the same thing but with an RGB fan and have it integrated into OpenRGB.  This is achieved using an Arduino Pro Micro and a 3D printed stand.  I chose the 120mm Corsair QL120 fan because it has lighting on both sides, but any 120mm ARGB fan with a 4-pin PWM fan connector should work.  The ARGB header is designed for Corsair fans so you may need to get a Corsair to ARGB 3-pin adapter or slightly modify the design to fit your fan.

## Features

* Turn any 120mm ARGB fan into a light show for your desk that keeps you cool
* Easily control fan speed and LED brightness using the control knob
* Integrates into OpenRGB (and any other software that supports the Corsair Commander Pro)
* Designs for USB bus-powered and 12V externally powered electronics available

## Software

The Arduino software is based on [CorsairLightingProtocol](https://github.com/Legion2/CorsairLightingProtocol).  Follow the instructions on the CorsairLightingProtocol GitHub page to set up your Arduino IDE before flashing.  Select the Commander Pro board.

## Electronics 

Information on assembling the electronics can be found in [Electronics](Doc/Electronics.md).

## 3D Models 

The 3D models are available in this repository or on [Thingiverse](https://www.thingiverse.com/thing:6655697).

The stand and grill are taken from Thingiverse user moogbass from [Fan Stand / Holder / Desktopfan / 120mm Fan W/ Grill](https://www.thingiverse.com/thing:4942322) which is licensed under the Creative Commons - Attribution license.  The Grill model provided in this repository is modified from that design to provide a rear cover to complement the front one that integrates the connection to the stand.

## Assembly

1. Install electronics into the bottom of the base.  Ports should line up with holes in the base as well as the screw holes for the PCBs.  Use 4 (USB-powered) or 8 (12V-powered) M2x5mm screws to secure the electronics into the base.

2. Cut 10 LED section of 60 LED/m WS2812B LED strip.  Solder on an appropriate connector.

3. Stick LED strip down in the front section of the base after peeling off the protective film.  You may have to bend up the ends slightly to get a snug fit.  Ensure it is stuck down evenly and flat, otherwise it may peel up over time.

4. Connect LED strip to the center 3-pin header on the electronics board.

5. Mount the fan to the fan mount (grill) using 4 fan screws.

6. Install the knob (with wires pre-soldered) to the top of the base.

7. Feed the fan wires through the hole in the top of the base.

8. Mount the fan stand to the top of the base using 2 M2x8mm screws.

9. Mount the fan mount into the fan stand using 2 M3x15mm bolts.

10. Connect the fan's ARGB and PWM headers and the knob connector to the electronics board.

11. Organize cables so that they don't cover up the LED strip, then put the top and bottom of the base together.

12. Use 4 M2x8mm screws from the bottom of the base to secure the top of the base to the bottom.